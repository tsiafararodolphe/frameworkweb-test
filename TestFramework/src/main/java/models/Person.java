package models;

import bind.annotation.Url;
import mapping.ModelView;

import java.util.Date;

public class Person {
    private int id;
    private String name;
    private Date dateNaissance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Url("person/save")
    public ModelView save(){
        ModelView modelView = new ModelView();
        modelView.setTarget("/home.jsp");
        modelView.addData("person", this);
        return modelView;
    }
}
